class CreateComputings < ActiveRecord::Migration
  def change
    create_table :computings do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
