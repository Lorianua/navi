class CreateSetellites < ActiveRecord::Migration
  def change
    create_table :setellites do |t|
      t.string  :name
      t.float   :x
      t.float   :y
      t.float   :z
      t.float   :R
      t.references :computing, index: true

      t.timestamps null: false
    end
  end
end
