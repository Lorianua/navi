require 'matrix'

class Computing < ActiveRecord::Base
  has_many :setellites, dependent: :destroy

  def solve_first
  	stls = []
  	self.setellites.each do |s|
  	  stls.push(s) 
  	end
  	a_coord =[[stls[0].x, stls[0].y, stls[0].z],
             [stls[1].x, stls[1].y, stls[1].z],
             [stls[2].x, stls[2].y, stls[2].z],
             [stls[3].x, stls[3].y, stls[3].z]]
    p = [stls[0].R, stls[1].R, stls[2].R, stls[3].R]

	coords = [0, 0, 0, 0]
	errors = [100, 100, 100, 100]
	eps = 0.01
	c = 299792458

	while errors[0] > eps || errors[1] > eps || errors[2] > eps || errors[3] > eps
	  begin
	    x1 = Math.sqrt(p[0]**2 - (coords[1]-a_coord[0][1])**2 - (coords[2]-a_coord[0][2])**2 - (coords[3]*c)**2) - a_coord[0][0]
	    x2 = -(Math.sqrt(p[0]**2 - (coords[1]-a_coord[0][1])**2 - (coords[2]-a_coord[0][2])**2 - (coords[3]*c)**2) - a_coord[0][0])
	  rescue
	  	x1 = Math.sqrt((p[0]**2 - (coords[1]-a_coord[0][1])**2 - (coords[2]-a_coord[0][2])**2 - (coords[3]*c)**2).abs) - a_coord[0][0]
	    x2 = -(Math.sqrt((p[0]**2 - (coords[1]-a_coord[0][1])**2 - (coords[2]-a_coord[0][2])**2 - (coords[3]*c)**2).abs) - a_coord[0][0])
	  end

	  begin
	  	y1 = Math.sqrt(p[1]**2 - (coords[0]-a_coord[1][0])**2 - (coords[2]-a_coord[1][2])**2 - (coords[3]*c)**2) - a_coord[1][1]
	    y2 = -(Math.sqrt(p[1]**2 - (coords[0]-a_coord[1][0])**2 - (coords[2]-a_coord[1][2])**2 - (coords[3]*c)**2) - a_coord[1][1])
	  rescue
	  	y1 = Math.sqrt((p[1]**2 - (coords[0]-a_coord[1][0])**2 - (coords[2]-a_coord[1][2])**2 - (coords[3]*c)**2).abs) - a_coord[1][1]
	    y2 = -(Math.sqrt((p[1]**2 - (coords[0]-a_coord[1][0])**2 - (coords[2]-a_coord[1][2])**2 - (coords[3]*c)**2).abs) - a_coord[1][1])
	  end

	  begin
	  	z1 = Math.sqrt(p[2]**2 - (coords[1]-a_coord[2][1])**2 - (coords[0]-a_coord[2][1])**2 - (coords[3]*c)**2) - a_coord[2][2]
	    z2 = -(Math.sqrt(p[2]**2 - (coords[1]-a_coord[2][1])**2 - (coords[0]-a_coord[2][1])**2 - (coords[3]*c)**2) - a_coord[2][2])
	  rescue
	  	z1 = Math.sqrt((p[2]**2 - (coords[1]-a_coord[2][1])**2 - (coords[0]-a_coord[2][1])**2 - (coords[3]*c)**2).abs) - a_coord[2][2]
	    z2 = -(Math.sqrt((p[2]**2 - (coords[1]-a_coord[2][1])**2 - (coords[0]-a_coord[2][1])**2 - (coords[3]*c)**2).abs) - a_coord[2][2])
	  end

	  begin
	  	t1 = Math.sqrt((p[3]**2 - (coords[1]-a_coord[3][1])**2 - (coords[2]-a_coord[3][2])**2 - (coords[0]-a_coord[3][0])**2)/c**2)
	    t2 = -(Math.sqrt((p[3]**2 - (coords[1]-a_coord[3][1])**2 - (coords[2]-a_coord[3][2])**2 - (coords[0]-a_coord[3][0])**2)/c**2))
	  rescue
	  	t1 = Math.sqrt((p[3]**2 - (coords[1]-a_coord[3][1])**2 - (coords[2]-a_coord[3][2])**2 - (coords[0]-a_coord[3][0])**2).abs/c**2)
	    t2 = -(Math.sqrt((p[3]**2 - (coords[1]-a_coord[3][1])**2 - (coords[2]-a_coord[3][2])**2 - (coords[0]-a_coord[3][0])**2).abs/c**2))
	  end

	  if (x1-coords[0]).abs < (x2-coords[0]).abs
	  	errors[0] = x1 - coords[0]
	  	coords[0] = x1
	  else
	  	errors[0] = x2 - coords[0]
	  	coords[0] = x2
	  end

	  if (y1-coords[1]).abs < (y2-coords[1]).abs
	  	errors[1] = y1 - coords[1] 
	  	coords[1] = y1
	  else
	  	errors[1] = y2 - coords[1]
	  	coords[1] = y2
	  end

	  if (z1-coords[2]).abs < (z2-coords[2]).abs
	  	errors[2] = z1 - coords[2] 
	  	coords[2] = z1
	  else
	  	errors[2] = z2 - coords[2]
	  	coords[2] = z2
	  end

	  if (t1-coords[3]).abs < (t2-coords[3]).abs
	  	errors[3] = t1 - coords[3] 
	  	coords[3] = t1
	  else
	  	errors[3] = t2 - coords[3]
	  	coords[3] = t2
	  end
	end
	return coords
  end

  def solve_second
  	stls = []
  	self.setellites.each do |s|
  	  stls.push(s) 
  	end
  	a_coord = Matrix[[stls[0].x, stls[0].y, stls[0].z],
             [stls[1].x, stls[1].y, stls[1].z],
             [stls[2].x, stls[2].y, stls[2].z],
             [stls[3].x, stls[3].y, stls[3].z]]
    p = Matrix[[stls[0].R, stls[1].R, stls[2].R, stls[3].R]]

	x = 0
	y = 0
	z = 0
	t = 0

	epsl = 10
	dx = epsl * 10
	dy = epsl * 10
	dz = epsl * 10
	dt = epsl * 10

	apred = Matrix[[x, y, z, t]]
	da = Matrix[[dx, dy, dz, dt]]

	while da[0,0] > epsl || da[0,1] > epsl || da[0,2] > epsl || da[0,3] > epsl 
	  a = Matrix[[x, y, z, t]]
	  r = Matrix[]
	  pr = Matrix[]
	  d = Matrix[]
	  l = Matrix[]
	  0.upto(3) { r = Matrix.rows(r.to_a << [x, y, z, t]) }
	  input_pr = []
	  input_l = []
	  0.upto(3) do |i|
	  	input_pr.push(Math.sqrt((a_coord[i,0]-r[0,0])**2+(a_coord[i,1]-r[0,1])**2+(a_coord[i,2]-r[0,2])**2))
	  end
	  pr = Matrix.rows(pr.to_a << input_pr)
	  0.upto(3) do |i|
	  	input_d = []
	  	0.upto(2) do |j|
	  	  input_d.push((a_coord[i,j]-r[0,i])/pr[0,i])
	  	end
	  	d = Matrix.rows(d.to_a << input_d)
	  	input_l.push(pr[0,i]-p[0,i])
	  end
	  l = Matrix.rows(l.to_a << input_l)
	  w1 = Matrix.identity(4)

	  dr = (d.transpose*w1*d).inverse*d.transpose*w1*l.transpose
	  a1 = []
	  0.upto(2) do |i|
	  	a1.push(a[0,i] + dr[i,0])
	  end
	  a1[3] = a[0,3]
	  x, y, z = a1[0], a1[1], a1[2]
	  dx = (a1[0] - apred[0,0]).abs
	  dy = (a1[1] - apred[0,1]).abs
	  dz = (a1[2] - apred[0,2]).abs
	  dt = (a1[3] - apred[0,3]).abs
	  new_da = Matrix.rows([] << [dx, dy, dz, dt])
	  da = new_da
	  new_apred = Matrix.rows([] << a1)
	  apred = new_apred
	end
	return apred.to_a
  end
end
