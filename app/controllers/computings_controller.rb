class ComputingsController < ApplicationController
  before_action :get_computing, only: [:show, :edit, :update, :destroy, :count_first, :count_second]

  def count_first
    @answer = @computing.solve_first
  end

  def count_second
    @answer = @computing.solve_second
  end

  def index
  	@computings = Computing.all
  end

  def show
    @setellites = @computing.setellites
  end

  def new
    @computing = Computing.new
  end

  def edit
  end

  def create
    @computing = Computing.create(computing_params)
    redirect_to computings_path
  end

  def update
    @computing.update_attributes(computing_params)
    redirect_to @computing
  end

  def destroy
    @computing.destroy
  end

  private

  def computing_params
  	params.require(:computing).permit(:name)
  end

  def get_computing
    @computing = Computing.find(params[:id])
  end

end
