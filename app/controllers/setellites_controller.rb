class SetellitesController < ApplicationController

  before_action :get_setellite, only: [:show, :edit, :update, :destroy]

  def show
  end

  def new
    get_computing
    @setellite = @computing.setellites.build
  end

  def edit
  end

  def create
    get_computing
    @setellite = @computing.setellites.create(setellite_params)
    redirect_to @computing
  end

  def update
    @setellite.update_attributes(setellite_params)
    redirect_to([@setellite.computing, @setellite])
  end

  def destroy
    @setellite.destroy
    redirect_to @computing
  end

  private

  def setellite_params
    params.require(:setellite).permit(:name, :x, :y, :z, :R)
  end

  def get_computing
    @computing = Computing.find(params[:computing_id])
  end

  def get_setellite
    get_computing
    @setellite = @computing.setellites.find(params[:id])
  end

end
